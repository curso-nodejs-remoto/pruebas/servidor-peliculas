const express = require("express");

const rutas = express.Router();

rutas.get("/", (req, res, next) => {
  res.send("Aquí irá el listado de directores");
});

module.exports = rutas;
