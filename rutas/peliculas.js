require("dotenv").config();
const express = require("express");
const debug = require("debug")("peliculas:rutas-peliculas");
const chalk = require("chalk");
const { body, validationResult } = require("express-validator");

const peliculasJSON = require("../peliculas.json");

const rutas = express.Router();

rutas.get("/", (req, res, next) => {
  debug(chalk.blue("Se ha pedido el listado de películas"));
  res.json(peliculasJSON);
});

rutas.get("/ver/:id", (req, res, next) => {
  const id = +req.params.id;
  const peliculas = peliculasJSON.peliculas;
  const pelicula = peliculas.find(pelicula => pelicula.id === id);
  debug(chalk.blue(`Se ha pedido el detalle de la película ${id}`));
  if (!pelicula) {
    const error = new Error(`No hay ninguna película con la id ${id}`);
    error.codigoStatus = 404;
    return next(error);
  }
  res.json(pelicula);
});

rutas.post("/crear", [
  body("titulo").isLength({ min: 1 })
], (req, res, next) => {
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    const error = new Error("La película no es correcta");
    error.codigoStatus = 400;
    next(error);
    return;
  }
  let pelicula = req.body;
  pelicula = {
    ...pelicula,
    id: getNuevaId(peliculasJSON.peliculas),
    vista: false,
    valoracion: 0
  };
  peliculasJSON.peliculas.push(pelicula);
  debug(chalk.blue("Se ha enviado una película para ser creada"));
  res.json(pelicula);
});

rutas.delete("/borrar/:id", (req, res, next) => {
  const id = +req.params.id;
  let peliculas = peliculasJSON.peliculas;
  const pelicula = peliculas.find(pelicula => pelicula.id === id);
  if (!pelicula) {
    const error = new Error(`No hay ninguna película con la id ${id}`);
    error.codigoStatus = 404;
    return next(error);
  }
  peliculas = peliculas.filter(pelicula => pelicula.id !== id);
  peliculasJSON.peliculas = peliculas;
  res.json({ id });
});

function getNuevaId(elementos) {
  return elementos[elementos.length - 1].id + 1;
}

module.exports = rutas;
