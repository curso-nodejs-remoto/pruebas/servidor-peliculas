require("dotenv").config();
const debug = require("debug")("peliculas:root");
const chalk = require("chalk");
const morgan = require("morgan");
const express = require("express");
const bodyParser = require("body-parser");

const rutasPeliculas = require("./rutas/peliculas");
const rutasDirectores = require("./rutas/directores");

const app = express();

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(express.static("public"));
app.get("/", (req, res, next) => {
  res.redirect("/peliculas");
});
app.use("/peliculas", rutasPeliculas);
app.use("/directores", rutasDirectores);
app.use((req, res, next) => {
  const error = new Error("La URL solicitada no existe");
  error.codigoStatus = 404;
  next(error);
});
app.use((err, req, res, next) => {
  const codigoStatus = err.codigoStatus || 500;
  const mensaje = codigoStatus === 500 ? "Ha ocurrido un error general" : err.message;
  debug(chalk.red(err.message));
  const respuesta = { mensaje };
  res.status(codigoStatus).json(respuesta);
});

const puerto = process.env.PELICULAS_PUERTO || 3000;

const server = app.listen(puerto, () => {
  debug(chalk.yellow(`Servidor escuchando en el puerto ${puerto}`));
});

server.on("error", err => {
  debug(chalk.red("Ha ocurrido un error al iniciar el servidor"));
  if (err.code === "EADDRINUSE") {
    debug(chalk.red(`El puerto ${puerto} ya está siendo usado por otra aplicación.`));
  }
});
